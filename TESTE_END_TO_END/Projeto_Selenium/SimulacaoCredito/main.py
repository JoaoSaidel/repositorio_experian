import homepage

arquivotxt = "resultado.txt"
homepage.excluir(arquivotxt)

homepage.abrir()

#Validar 1° Cenario/ Dados de entrada: Simulação de: R$ 1.000 em 6 vezes / Valor esperado: 6x R$ 271,27
homepage.executar_testes(1000,6,'271,27'); 

#Validar 2° Cenario/ Dados de entrada: Simulação de: R$ 4.000 em 12 vezes / Valor esperado: 12x R$ 478,49
homepage.executar_testes(4000,12,'478,49')

#Validar 3° Cenario/  Dados de entrada: Simulação de: R$ 6.000 em 24 vezes / Valor esperado: 24x R$ 347,89
homepage.executar_testes(6000,24,'347,89')

homepage.fechar()