import requests

api_url = 'https://8dac9f4e-fcb2-4e8f-857a-e4ed3497a0d8.mock.pstmn.io/bank/'

# Dados do novo item a ser adicionado
new_item = {
    'id': 25,
    'banco': 'Novo Banco',
    'estadoAtuacao': 'SP',
    'juros': '10,00%'
}

# Fazendo a requisição POST para adicionar o novo item
response = requests.post(api_url, json=new_item)

# Verificando o código de status da resposta
if response.status_code == 201:
    # O novo item foi adicionado com sucesso (código 201)
    print("Novo item adicionado com sucesso.")

    # Fazendo uma nova requisição GET para verificar o item cadastrado
    response = requests.get(api_url)
    if response.status_code == 200:
        # A resposta foi bem-sucedida (código 200)
        data = response.json()

        # Procurando o novo item na resposta
        for item in data:
            if item['id'] == new_item['id']:
                print("Item encontrado:")
                print(item)
                break
        else:
            print("Item não encontrado.")
    else:
        # A requisição GET falhou
        print(f'Falha ao acessar a API. Código de status: {response.status_code}')
else:
    # A requisição POST falhou
    print(f'Falha ao adicionar o novo item. Código de status: {response.status_code}')
