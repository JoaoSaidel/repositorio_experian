import requests

api_url = 'https://8dac9f4e-fcb2-4e8f-857a-e4ed3497a0d8.mock.pstmn.io/bank/'

# Fazendo a requisição GET à API
response = requests.get(api_url)

# Verificando o código de status da resposta
if response.status_code == 200:
    # A resposta foi bem-sucedida (código 200)
    data = response.json()  # Converte a resposta em formato JSON para um dicionário Python
    
    desired_id = 8  # Valor do id desejado
    found_item = None
    
    # Percorre os itens da API e verifica se o valor do id corresponde ao valor desejado
    for item in data:
        if item['id'] == desired_id:
            found_item = item
            break
    
    if found_item:
        print("Item encontrado:")
        print(found_item)
    else:
        print(f"Nenhum item encontrado com o id {desired_id}.")
else:
    # A requisição falhou
    print(f'Falha ao acessar a API. Código de status: {response.status_code}')
