import os
import time
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.chrome.service import Service
from selenium.webdriver import ActionChains

class Inicializadora:
    def __init__(self):
        self.url = "https://www.serasa.com.br/ecred/simulador/"
        self.driver_service = Service(executable_path="/.chromedrive.exe")
        self.driver = webdriver.Chrome(service=self.driver_service)
        self.driver.maximize_window()

    def abrir_site(self):
        try:
            self.driver.get(self.url)
            WebDriverWait(self.driver, 10).until(EC.presence_of_element_located((By.ID, 'slider-range'))) 
            self.SliderValorEmprestimo = self.driver.find_element(By.ID,'slider-range')
            self.SliderParcelas = self.driver.find_element(By.ID,'slider-range-month')           
        except Exception as e:
            print(f"Erro ao abrir o navegador: {str(e)}")

    def fecha_site(self):
        try:
          self.driver.quit()
        except Exception as e:
          print(f"Erro ao fechar o navegador: {str(e)}")

    def excluir_arquivo(self, nome_arquivo):
        if os.path.exists(nome_arquivo):
            try:
                os.remove(nome_arquivo)
                print(f"Arquivo '{nome_arquivo}' excluído com sucesso.")
            except Exception as e:
                print(f"Erro ao excluir o arquivo '{nome_arquivo}': {str(e)}")
        else:
            print(f"O arquivo '{nome_arquivo}' não existe.")

    def validar_cenario(self, valorEntrada, qtdVezes, valorEsperado):
        self.chama_seleciona_slider(valorEntrada, qtdVezes)
        self.validar_valores(qtdVezes, valorEsperado)

    def chama_seleciona_slider(self, valorEntrada, qtdVezes):
        self.seleciona_slider(valorEntrada,qtdVezes,'SliderValorEmprestimo')
        self.seleciona_slider(valorEntrada,qtdVezes,'SliderParcelas')

    def seleciona_slider(self, valorEntrada, qtdVezes, tiposlider):
        if tiposlider == 'SliderValorEmprestimo':
          action = ActionChains(self.driver)
          action.click_and_hold(self.SliderValorEmprestimo).move_by_offset(self.retorna_valor_slider(valorEntrada), 0).release().perform()
          time.sleep(2)
        elif tiposlider == 'SliderParcelas':
          action = ActionChains(self.driver)
          action.click_and_hold(self.SliderParcelas).move_by_offset(self.retorna_parcela_slider(qtdVezes), 0).release().perform() 
          time.sleep(2)

    def retorna_valor_slider(self, valorEntrada):
        try:
            if valorEntrada == 1000:
                resultado = -200
                return resultado
            elif valorEntrada == 4000:
                resultado = -157
                return resultado
            elif valorEntrada == 6000:
                resultado = -128
                return resultado
            else: 
                raise ValueError('Infelizmente, valor ainda não disponível')
        except ValueError as e:
            print(str(e))
            return 0

    def retorna_parcela_slider(self, qtdVezes):
        try:
            if qtdVezes == 6:
               resultado = -170
               return resultado
            elif qtdVezes == 12:
                resultado = -150
                return resultado
            elif qtdVezes == 24:
                resultado = -40
                return resultado
            else:
                raise ValueError('Infelizmente, valor ainda não disponível')
        except ValueError as e:
            print(str(e))
            return 0

    def validar_valores(self, qtdVezes, valorEsperado):
        valor_em_tela = self.driver.find_element(By.ID, 'price-number').text
        parcelas_em_tela = self.driver.find_element(By.ID, 'month-amount').text

        parcelas_em_tela = self.extrair_apenas_numeros(parcelas_em_tela)
        qtdVezesStr = str(qtdVezes)

        resultados = ""

        if valor_em_tela == valorEsperado:     
            resultados = f"O valor em tela: ({valor_em_tela}) esta correto.\n"
        else:
            resultados = f"Erro: O valor em tela ({valor_em_tela}) nao corresponde ao valor esperado ({valorEsperado}).\n"

        if parcelas_em_tela == qtdVezesStr:
            resultados += (f"O numero de parcelas em tela: ({parcelas_em_tela}) esta correto.\n")
        else:
            resultados += (f"Erro: O numero de parcelas em tela ({parcelas_em_tela}) nao corresponde ao valor esperado ({qtdVezes}).\n")
     
        self.escrever_resultados_em_arquivo(resultados, valorEsperado, qtdVezes)

    def escrever_resultados_em_arquivo(self, resultados,valorEsperado,qtdVezes):
        with open('resultado.txt', 'a') as arquivo:
            tex = f"Resultado do Teste, com as parcelas: {valorEsperado} e {qtdVezes} vezes:\n" 
            arquivo.write(tex)
            for resultado in resultados:
                arquivo.write(resultado)
            arquivo.write('\n')

    def extrair_apenas_numeros(self, texto):
        numeros = ''.join(filter(str.isdigit, texto))
        return numeros

    def excluir_arquivo(self, nome_arquivo):
        if os.path.exists(nome_arquivo):
            try:
                os.remove(nome_arquivo)
                print(f"Arquivo '{nome_arquivo}' excluído com sucesso.")
            except Exception as e:
                print(f"Erro ao excluir o arquivo '{nome_arquivo}': {str(e)}")
        else:
            print(f"O arquivo '{nome_arquivo}' não existe.")

projeto = Inicializadora()

def abrir():
    projeto.abrir_site()

def fechar():
    projeto.fecha_site()

def executar_testes(valor, vezes, preco):
    projeto.validar_cenario(valor, vezes, preco)

def excluir (nome_arquivo):
    projeto.excluir_arquivo(nome_arquivo)

